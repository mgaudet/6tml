/* Copyright 2012 Matthew Gaudet */


// Provides the ability to play out a game sequence
// A replay list is an array of js objects with at least the following
// properties: 
// -- x coord 
// -- y coord
// -- played colour to play
//
// The x y coordinates should be possible to look up in the board
// to get a poly (see replay())
//
// To use, I suggest dumping undo stacks of games you wish to replay.
// Then, ask replay_function for an incremental replay function and bind it 
// to a button.

function dumpUndo(b) { 
    var undo  = b.undoStack; 
    var todump = new Array;
    for (var i = 0; i < undo.length; i++) {
        var k = undo[i];
        var d = { x: k.x, y: k.y, played: k.played};
        todump.push(d); 
    }
    return JSON.stringify(todump)
}

// replays element e.
function replay(b,e) {  
    var x = e.x;
    var y = e.y;
    var c = e.played;

    p = b.polys[x+","+y];
    b.play(x,y,c,p);
    $(p).attr('fill',c);
}

//Returns a function suitible for stepping through some replays
function replay_function(board,replays) { 
    return function () { 
        var e = replays.shift();
        replay(board,e);
    }
}

    


