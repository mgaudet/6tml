/* Copyright 2012 Matthew Gaudet. See LICENCE.txt at root of directory
 *
 * Hex board in JS
 */

/* Creates a board of size N,M and
 * returns it 
 *
 *
 */

//Create error class inheriting from Error
function HexBoardError() { 
}
HexBoardError.prototype = new Error();

/* Returns a HexBoard object */
var createHexBoard = function(N,M,width,height) { 
    var board = {};
    board.width  = width;
    board.height = height;
    board.n = N; //Y 
    board.m = M; //X  
    board.empty = "lightGrey";
    board.white = "white";
    board.black = "black";
    board.undoStack = new Array;
    board.redoStack = new Array;
    board.polys = {}

    //Tile size. Can override before calling draw to change tile size
    board.exp = 44;
    
    //Stored reference to poly which indicates side to play.
    board.sidePoly = "";

    //To Play = player to move.
    board.toplay = board.black; 

    //Game state backing array.
    board.squares = new Array(board.m+2);
    for (var i =0; i < board.m+2; i++) { 
        board.squares[i] = new Array(board.n+2);
        for (var j =0; j < board.n+2; j++) { 
            board.squares[i][j] = board.empty 
        }
    }

    //Initialize black
    for (var i = 0; i < board.n+1; i++) {
        var tx = i;
        var ty = 0;
        var bx = i;
        var by = board.m+1;
        board.squares[tx][ty] = board.black;
        board.squares[bx][by] = board.black;
    }

    //Initialize white 
    for (var i = 0; i < board.m+1; i++) {
        var lx = 0;
        var ly = i;
        var rx = board.n+1;
        var ry = i;
        board.squares[lx][ly] = board.white;
        board.squares[rx][ry] = board.white;
    }

    board.squares[0][0] = board.empty;
    board.squares[board.n+1][board.m+1] = board.empty;
    board.squares[board.n+1][0] = board.empty;
    board.squares[0][board.m+1] = board.empty;
    
    board.play = function (x,y,colour,poly) { 
        if (x > board.n || y > board.m || x < 0 || y < 0) { 
            throw new HexBoardError("Out of bounds play: "+x+","+y);
        }

        if (colour != board.white && colour != board.black) { 
            throw new HexBoardError("Can't play colour"+colour);
        }
        if (board.squares[x][y] != board.empty) { 
            throw new HexBoardError("Board position: "+x+","+y+" occupied");
        }
        board.undoStack.push({x: x, y: y, c: board.squares[x][y], p: poly, played: colour});
        board.redoStack = new Array;
        board.squares[x][y] = colour;
        
        var k = board.connected();
        if (k) { 
            alert(k+" is the winner!");
        }

        return true;
    };

    //Undo function; Pops a move off the stack. 
    // Calls styleCB if the play saved a poly.
    board.undo = function() {
        var o = board.undoStack.pop(); 
        var x = o.x;
        var y = o.y; 
        var c = o.c;
        var p = o.p;
 
        board.toplay = board.squares[x][y];
        board.squares[x][y] = o.c;
        if (p) { 
            $(p).attr('fill',o.c);
            board.sideStyle(board.toplay);
        }

    }

    //styleCB is a callback function which should 
    //update the style of the displayed hex.
    // Takes as it's argument the colour for the new fill.
    board.peoplePlay = function(poly,x,y,styleCB) { 
       board.play(x,y,board.toplay,poly); 
       if (board.toplay == board.white) {
           board.toplay =  board.black;
       } else { 
           board.toplay = board.white;
       }
       board.sideStyle(board.toplay);
       styleCB(board.squares[x][y]); 
    }

    board.cb = function() { 
        hexcallback(this,board);
    }

    //Draw function; Called once to set up the board.
    board.draw = function (svg,callback) {
        var call = callback ? callback : board.cb;
        board.sidePoly =  draw(board,svg,call);
        $(board.sidePoly).bind('click',function() { board.undo() });
        board.sideStyle = function (c) {
            $(board.sidePoly).attr('fill',c);
        };
        board.sideStyle(board.toplay);
    };


    //call with start and goal as [x,y] arraypairs.
    board.bfs = function(side,start,goal,succ,result) {
        //console.log("BFS Start: "+start+" goal: "+goal+ " trying side" + side);
        //push+shift == queueu
        var queue = new Array();
        var visited = {};

        queue.push(start);
        if (board.squares[start[0]][start[1]] != side) { 
            console.log("INVALID: START COORDS");
        }

        while (queue.length != 0) { 
            var elem = queue.shift();
            //console.log("elem: "+elem);
            visited[elem.join()] = true;
            successors = succ(elem).filter(function (e) { 
                if (visited[e.join()]) { return false; } 
                else {
                    x = e[0];
                    y = e[1];
                    try {
                        //console.log("checking "+x+","+y);
                        if (board.squares[x][y] == side) {
                            return true;
                        }
                    } catch (e) { //likely should be ensuring this is actually
                                  // an array value exception
                        //console.log("caught: "+e);
                    };
                    return false; 
                }
            });
            //console.log("Successors: "+successors);

            for (var i = 0; i < successors.length; i++) { 
                queue.push(successors[i]);
                if (successors[i].join() == goal.join()) {
                   //console.log("FOUND GOAL!");
                   return true; 
                }
            }
        }
    }

    //generates successor list;
    board.successor = function (k) {
        major = k[0];
        minor = k[1];
        return [[major,minor-1], [major+1,minor-1 ], [major +1,minor], 
                [major, minor+1],[major-1,minor +1], [major -1,minor]];
    }



    //Returns hb.black or hb.white depending 
    //on who has made a connection
    board.connected = function () { 
        var w = board.empty;
        console.log("Start checking black");
        if (board.bfs(board.black,[1,0],[1,board.n+1],
                    board.successor,[])) {
            return board.black;
        } 
    
        console.log("Start checking white");
        if (board.bfs(board.white,[0,1],[board.m+1,1],
                    board.successor,[])) {
            return board.white;
        }

        return;
    };


    return board;
};


function hexcallback(poly,board) { 
    try { 
        board.peoplePlay(poly,poly.x,poly.y,function(colour) {
            $(poly).attr('fill',colour);
        });
    } catch (e) { 
        if (e instanceof HexBoardError) { 
            alert(board.toplay+" can't play there. Try elsewhere");
        } else { 
            throw e;
        }
    }
}

function draw(board,svg,callback) { 
    //Magic constant 0.4 found through experimentation.
    var t = svg.group("shift",{transform: 'translate(0,'+board.height*0.40+')'});
    var g = svg.group(t,"Hexboard",{transform: 'rotate(-30,0,0)'});
    for (var i = 0; i < board.m+2; i++) { 
        for (var j = 0; j < board.n+2; j++) { 
            var p = dh(svg,g,i,j,board.exp,callback,board);
            board.polys[i+","+j] = p;
        }
    }
    
    return dh(svg,g,-3,board.n,board.exp);
}

//Draws a hex at the LOGICAL cooridinates X,Y. 
function dh(svg,g,x,y,exp,func,board) { 
            var yoff = exp;
            var xoff = exp;
            var xshift = y * Math.sqrt(3) * exp / 2; 
            var poly = drawHex(svg,g, [xshift + xoff+x*Math.sqrt(3)*exp,yoff+exp*y*1.5], exp,func,x,y,board);
            return poly;
}


function drawHex(svg,g,origin,expand,func,i,j,board) { 
    var ox = origin[0];
    var oy = origin[1];
    var rt3 = 1.732050808*expand;
    var pts = [[ox-expand,oy]            ,[ox-(expand/2),oy+(rt3/2)],
               [ox+(expand/2),oy+(rt3/2)],[ox+(expand),oy],
               [ox+(expand/2),oy-(rt3/2)],[ox-(expand/2),oy-(rt3/2)]];
   
    var fill_col = 'lightgrey';
    if (board) { 
        if (board.squares[i][j] != board.empty) {
            fill_col = board.squares[i][j]; 
        } 
    }
    var poly = svg.polygon(g,pts,{fill: fill_col,strokeWidth: '3',stroke:'black', transform: 'rotate(-30,'+ox+','+oy+')'});
    poly['x'] = i;
    poly['y'] = j;
    $(poly).bind('click',func);
    return poly;
}



