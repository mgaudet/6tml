function RunTest(){

    module("Basic HexBoard Tests");
    test("Can Get Board", function() { 
        var hb = createHexBoard(10,10);
        ok(hb,"Create hex board");
    });
    test("Test Board Plays", function()
    {
       var hb = createHexBoard(10,10); 
       ok(hb,"create hex board");
       ok(hb.play(1,1,hb.black,"Play 1,1 black"));
       raises(function () { hb.play(1,1,hb.black)},HexBoardError,"Test double play");
       equal(hb.squares[1][1],hb.black,"Saved play colours");
    });

    test("Test Undo Stack", function() { 
        var hb = createHexBoard(4,4);
        hb.play(1,1,hb.black); 
        equal(hb.squares[1][1],hb.black,"Saved play");
        hb.undo();
        equal(hb.squares[1][1],hb.empty,"restored play!");
    });

    test("Test DFS", function() { 
        var hb = createHexBoard(4,4); 
        hb.play(2,1,hb.black);
        hb.play(2,2,hb.black);
        hb.play(2,3,hb.black);
        hb.play(2,3,hb.black);
        var connection = hb.connected(); 
        equal(connection,hb.black,"Connected");
    });
}
