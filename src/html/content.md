The Game of Hex
==============

Hex is a board game which was invented twice. First by [Piet Hein][ph] in 1942 
and second by [John Nash][jn] in 1948. 

Played on a hexagonal board, play alternates between two players, say Black 
and White. The two opposing pairs of sides are coloured according to the player
colours. For each side, White and Black, the goal is to produce a connection 
between the two correspondingly coloured sides.

Once a stone is played, it cannot be removed. 

>Below is a five by five board: Try playing a game against yourself. **The hex 
>tile disconnected from the board indicates whose turn it is to play. Clicking 
>it will undo the last play.**

<center><div id="board1"></div></center>

Depending on how long you played with this little board, you mauy or may not 
have discovered that whomever goes first (Black in this case) seems to 
always be able to win. 

In most human play, people use a so called _swap rule_ to give a little more 
challenge to the play. By the swap rule, whomever plays second may chose to 
either play a stone of their colour (in our case, White), or may choose to 
instead swap positions, and start playing as their opponent (Black). This forces 
the first player to consider playing not the strongest opening, but something 
more uncertain. 

> Try playing a friend again, using the swap rule. 
>
>Of course, playing on a 5x5 five board is less interesting. Here is a 13x13 board.

<center><div id="board2"></div></center>

The contents of this page are sourced almost entirely from [__A puzzling Hex 
primer__][php] (PDF) by Dr. Ryan Hayward.

The code which drives this page is available under the BSD licence on [Bitbucket][bb].

[ph]: https://en.wikipedia.org/wiki/Piet_Hein_(Denmark)
[jn]: https://en.wikipedia.org/wiki/John_Forbes_Nash%2C_Jr.
[php]: http://webdocs.cs.ualberta.ca/~hayward/papers/puzzlingHexPrimer.pdf
[bb]: https://bitbucket.org/mgaudet/6tml/wiki/Home
